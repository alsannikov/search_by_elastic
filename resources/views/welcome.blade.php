<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="flex-center position-ref full-height">
        <div class="content">

            <autocomplete
                ref="autocomplete"
                placeholder="Search"
                :source="distributionGroupsEndpoint"
                input-class="form-control"
                results-property="data"
                :results-display="formattedDisplay"
            >
            </autocomplete>
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>

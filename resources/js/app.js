require('./bootstrap');

window.Vue = require('vue');
window.Autocomplete = require('vuejs-auto-complete');

const app = new Vue({
    el: '#app',
    components: {
        Autocomplete,
    },
    methods: {
        distributionGroupsEndpoint (input) {
            return '/user/autocomplete?query=' + input
        },
        formattedDisplay (result) {
            return result.name
        }
    }
});

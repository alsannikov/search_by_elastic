<?php
namespace App\Console\Commands\Search;

use App\User;
use Illuminate\Console\Command;
use Elasticsearch\Client;

/**
 * Class ReindexCommand
 * @package App\Console\Commands\Search
 *
 * @property Client $client
 */
class ReindexCommand extends Command
{
    protected $signature = 'search:reindex';

    private $client;

    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    public function handle()
    {
        $this->client->deleteByQuery([
            'index' => 'user',
            'type' => 'user',
            'body' => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);

        foreach (User::cursor() as $user) {

            $this->client->index([
                'index' => 'user',
                'type' => 'user',
                'id' => $user->id,
                'body' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ],
            ]);
        }
    }
}

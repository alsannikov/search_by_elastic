<?php
namespace App\Console\Commands\Search;

use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;
use Elasticsearch\Client;

/**
 * Class InitCommand
 * @package App\Console\Commands\Search
 *
 * @property Client $client
 */
class InitCommand extends Command
{
    protected $signature = 'search:init';

    private $client;

    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    public function handle()
    {
        try {

            $this->client->indices()->delete([
                'index' => 'user'
            ]);
        } catch (Missing404Exception $e) {}

        $this->client->indices()->create([
            'index' => 'user',
            'body' => [
                'mappings' => [
                    'user' => [
                        '_source' => [
                            'enabled' => true,
                        ],
                        'properties' => [
                            'id' => [
                                'type' => 'integer',
                            ],
                            'name' => [
                                'type' => 'text',
                            ],
                            'email' => [
                                'type' => 'text',
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}

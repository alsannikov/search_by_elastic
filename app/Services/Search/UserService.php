<?php
namespace App\Services\Search;

use Elasticsearch\Client;

/**
 * Class User
 * @package App\Services\Search
 *
 * @property Client $client
 */
class UserService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $query
     * @return array
     */
    public function getByNameOrEmail(string $query): array
    {
        $queryFormatted = mb_strtolower(trim($query));
        $response = $this->client->search([
            'index' => 'user',
            'type' => 'user',
            'body' => [
                '_source' => ['id', 'name'],
                'size' => 10,
                'query' => [
                    'multi_match' => [
                        'query' => $queryFormatted,
                        'type' => 'phrase_prefix',
                        'fields' => ['name', 'email'],
                    ]
                ]
            ]
        ]);

        return array_column((array) $response['hits']['hits'], '_source');
    }
}

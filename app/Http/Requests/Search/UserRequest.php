<?php
namespace App\Http\Requests\Search;


use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'query' => 'required', 'string', 'max:255', 'alpha_dash'
        ];
    }
}

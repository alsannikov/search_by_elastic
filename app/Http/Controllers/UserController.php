<?php
namespace App\Http\Controllers;


use App\Http\Requests\Search\UserRequest;
use App\Services\Search\UserService;

/**
 * Class UserController
 * @package App\Http\Controllers
 *
 * @property UserService $service
 */
class UserController extends Controller
{
    private $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param UserRequest $request
     * @return array
     */
    public function autocomplete(UserRequest $request): array
    {
        return $this->service->getByNameOrEmail($request->get('query'));
    }
}

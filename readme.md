<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Тестовое задание

Автокомплит на основе ElasticSearch 

### Installation

- install dependencies (docker compose, make)
- `git clone git@bitbucket.org:alsannikov/search_by_elastic.git .`
- `make app-install`
- open [http://localhost](http://localhost)

<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/user/autocomplete', 'UserController@autocomplete')->name('user.autocomplete');
